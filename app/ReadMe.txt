Echipa Bidroid
Boloș Eduard Cristian
Tănăselea Roxana
				Trip Memories - Instructiuni de utilizare

	

1.Scop
    Scopul acestei aplicatii este de a ajuta utilizatorul sa isi imortalizeze experientele
    avute in timpul unei calatorii/vacante.Aplicatia trebuie sa ii permita userului  sa
    creeze un album foto care va fi populat cu fotografiile facute de catre acesta.

2.Functionalitati

2.1 Creare Album
    Pentru a putea organiza fotografiile realizate cu ajutorul aplicatiei, utilizatorul
    trebuie sa creeze, in prima instanta, un album in care sa poata salva instantaneele.
    Creearea de album se realizeaza prin apasarea primei iconite din Action Bar.
    Albumul :
		 -daca  este gol, fotografia de coperta va fi o pictograma reprezentativa
		 -altfel, cea mai recenta fotografie va fi coperta albumului
		 
2.2 Operatii CRUD pe Album
    Utilizatorul are posibilitatea de a realiza asupra albumului urmatoarele actiuni:
        2.2.1 Creeare Album
        2.2.2 Vizualizare continut album -  se realizeaza prin apasarea fotografiei
                                            de coperta a unui album
                                         -  daca este gol un mesaj corespunzator este afisat
                                         -  altfel, pozele continute pot fi vizionate
        2.2.3 Redenumire Album - se realizeaza prin apasarea lunga a unui album si selectarea
                                 optiunii corespunzatoare din Action Bar
                               - titlul albumului se poate modifica
        2.2.4 Stergere Album - se realizeaza prin apasarea lunga a unui album sau a mai multora,
                               si selectare optiunii corespunzatoare din Action Bar
                             - realizeaza atat stergerea de album, cat si a pozelor continute
                               de acesta
                             - pozele din album se sterg atat din baza de date, cat si de pe
                               memoria externa
						 
2.3 Realizare fotografie
    Utilizatorul poate  realiza fotografiile prin intermediul aplicatiei, cu ajutorul API -
    ului pentru Camera. Pentru a realiza aceasta actiune trebuie sa apese pe cea de-a iconita
    din Action Bar si apoi (dupa deschiderea camerei) sa apese pictograma de pe ecran.
    Fotografia :
			- va cuprinde in titlul initial, data la care a fost realizata
			- va fi salvata initial in ultimul album creat sau deschis

2.4 Salvare Fotografie in Album
    -initial, fotografia va fi salvata in ultimul album creat sau deschis de utilizator

2.5 Operatii Crud Pe Fotografie
	2.5.1 Creare Fotografie
	2.5.2 Vizualizare Fotografie - prin apasarea pe o fotografie
								 - fotografiile dintr-un album sunt vizionate paginat
								 - se poate naviga, intre fotografii, prin aplicarea gestului 
								   de slide
	2.5.3 Mutare Fotografie - se realizeaza prin apasarea lunga a unui fotografii si 
							  selectare optiunii corespunzatoare din Action Bar, apoi
							  se selecteaza albumul de destinatie, din lista de optiuni
							  afisata
							- este posibila selectarea a unei singure fotografii sau a mai 
							  multora pentru a fi mutate dintr-un album in altul
	2.5.4 Stergere Fotografie - se realizeaza prin apasarea lunga a unui fotografii si 
								selectare optiunii corespunzatoare din Action Bar
							  - este posibila stergerea mai multor fotografii simultan
							  -	datele se sterg atat din baza de date, cat si de pe memoria 
								externa

2.6 Vizualizare fotografie
    Utilizatorul poate vizualiza fotografiile dintr-un album paginat. Navigarea intre fotografii, se
    realizeaza prin gestul de slide

3.Detalii Tehnice
3.1 Fire de executie
	Toate operatiunile de I/O al loc pe un fir de executie separat fata de cel al interfetei cu
	utilizatorul, cu ajutorul unui IntentService

3.2 Securitate 
	Aplicatia cere permisiunea  : - de a accesa  camera
							      - de a scrie pe memoria externa fotografiile realizate

3.3 Limba UI
    Limba interfetei va fi in limba romana pentru telefoanele care au aceasta limba setata ca si limba
    de sistem, respectiv engleza pentru alta limba de sistem

4. Instructiuni de compilare
4.1 Cerinte tehnice
    Pentru compilare este nevoie de Android Build Tools versiunea 19.0.3, precum si SDK-ul Android
    API Level 19. Cele mai recente versiuni ale amandurora se pot descarca din Android SDK Manager.

4.2 Compilare din linia de comanda
    Proiectul se poate compila din linia de comanda folosind Gradle prin urmatoarea comanda:
    gradlew aR

4.3 Deschiderea intr-un mediu de dezvoltare
    Proiectul a fost dezvoltat in Android Studio versiunea 0.5.4. Pentru a se deschide in acest IDE,
    se merge la meniul File->Import Project, si se alege fisierul build.gradle al proiectului.

4.4 Fisierul de instalare
    APK-ul aplicatiei se afla in app/build/apk/app-release.apk
