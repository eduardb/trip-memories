package com.bidroid.tripmemories.app.provider;

import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.tjeannin.provigen.Constraint;
import com.tjeannin.provigen.InvalidContractException;
import com.tjeannin.provigen.ProviGenBaseContract;
import com.tjeannin.provigen.ProviGenProvider;
import com.tjeannin.provigen.annotation.Column;
import com.tjeannin.provigen.annotation.ContentUri;
import com.tjeannin.provigen.annotation.Contract;
import com.tjeannin.provigen.annotation.NotNull;

/**
 * Created by Edy on 11.04.2014.
 */
@Contract(version = 2)
public class TripMemoriesContentProvider extends ProviGenProvider {

    public static final String DATABASE_NAME = "tripmemories.db";

    public TripMemoriesContentProvider() throws InvalidContractException {
        super(new Class[] {AlbumsContract.class, PhotosContract.class}, DATABASE_NAME);
    }
}
