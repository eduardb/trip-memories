package com.bidroid.tripmemories.app.ui.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SimpleCursorAdapter;

import com.bidroid.tripmemories.app.R;
import com.bidroid.tripmemories.app.domain.Photo;
import com.bidroid.tripmemories.app.provider.AlbumsContract;
import com.bidroid.tripmemories.app.provider.PhotosContract;
import com.bidroid.tripmemories.app.provider.QueryAlbums;
import com.bidroid.tripmemories.app.provider.QueryPhotos;
import com.bidroid.tripmemories.app.service.PersistenceService;
import com.bidroid.tripmemories.app.ui.MainActivity;
import com.bidroid.tripmemories.app.ui.PhotosViewerActivity;
import com.bidroid.tripmemories.app.ui.adapter.PhotosAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Use the {@link PhotosFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class PhotosFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ALBUM_ID = "album_id";
    public static final int LOADER_PHOTOS = 1;
    public static final int LOADER_ALBUMS = 2;
    public static final String ORDER_DESC = " DESC";


    private String mAlbumId;
    private GridView mGridView;
    private PhotosAdapter mPhotosAdapter;
    private SimpleCursorAdapter mAlbumsAdapter;
    private View mEmptyView;

     /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param albumId Parameter 1.
     * @return A new instance of fragment PhotosFragment.
     */
    public static PhotosFragment newInstance(String albumId) {
        PhotosFragment fragment = new PhotosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ALBUM_ID, albumId);
        fragment.setArguments(args);
        return fragment;
    }
    public PhotosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mAlbumId = getArguments().getString(ARG_ALBUM_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(R.string.album_pictures);
        ((MainActivity) getActivity()).mHasAlbum = true;
        ((MainActivity) getActivity()).mInAlbum = true;
        getActivity().invalidateOptionsMenu();
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().getActionBar().setHomeButtonEnabled(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_photos, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mGridView = (GridView) view.findViewById(R.id.gridview);
        List<Photo> photos = new ArrayList<Photo>();

        mPhotosAdapter = new PhotosAdapter(getActivity(), photos);
        mGridView.setAdapter(mPhotosAdapter);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ArrayList<String> photos = new ArrayList<String>();

                for (int i = 0; i < mPhotosAdapter.getCount(); i++) {
                    photos.add(mPhotosAdapter.getItem(i).getPath());
                }

                Intent intent = new Intent(getActivity(), PhotosViewerActivity.class);
                intent.putExtra(PhotosViewerActivity.PHOTOS_LIST, photos);
                intent.putExtra(PhotosViewerActivity.PHOTOS_LIST_POSITION, position);
                startActivity(intent);
            }
        });

        mAlbumsAdapter = new SimpleCursorAdapter(getActivity(),
                android.R.layout.simple_list_item_single_choice,
                null,
                new String[]{AlbumsContract.ALBUM_NAME},
                new int[] {android.R.id.text1}, 0);

        mEmptyView = view.findViewById(R.id.emptyView);

        mGridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
        mGridView.setMultiChoiceModeListener(new MultiChoiceModeListener());

        restartLoader(LOADER_PHOTOS);
        restartLoader(LOADER_ALBUMS);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) getActivity()).mInAlbum = false;
        getActivity().invalidateOptionsMenu();
    }

    public void refreshEmptyView()
    {
        if(mPhotosAdapter.getCount() == 0)
        {
            mEmptyView.setVisibility(View.VISIBLE);
        }
        else
            mEmptyView.setVisibility(View.GONE);
    }

    private void restartLoader(int id) {
        getLoaderManager().restartLoader(id, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (getActivity() == null) {
            return null;
        }

        Loader<Cursor> loader = null;
        String selection = null;
        String[] selectionArgs = null;
        switch (id) {
            case LOADER_PHOTOS:
                selection = PhotosContract.ALBUM_ID + "=?";
                selectionArgs = new String[] {mAlbumId};
                loader = new CursorLoader(getActivity(),
                        PhotosContract.CONTENT_URI,
                        QueryPhotos.PROJECTION, selection, selectionArgs,
                        PhotosContract.DATE_CREATED + ORDER_DESC);
                break;
            case LOADER_ALBUMS:
                loader = new CursorLoader(getActivity(),
                        AlbumsContract.CONTENT_URI,
                        QueryAlbums.PROJECTION, selection, selectionArgs,
                        AlbumsContract.DATE_CREATED + ORDER_DESC);
                break;
        }

        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (getActivity() == null) {
            return;
        }

        if (cursor != null && !cursor.isClosed()) {
            switch (cursorLoader.getId()) {
                case LOADER_PHOTOS:
                    List<Photo> photos = new ArrayList<Photo>();
                    while (cursor.moveToNext()) {
                        Photo photo = new Photo();
                        photo.setId(cursor.getLong(QueryPhotos._ID));
                        photo.setAlbumId(cursor.getLong(QueryPhotos.ALBUM_ID));
                        photo.setDateCreated(cursor.getLong(QueryPhotos.DATE_CREATED));
                        photo.setPath(cursor.getString(QueryPhotos.PHOTO_PATH));
                        photos.add(photo);
                    }
                    mPhotosAdapter.clear();
                    mPhotosAdapter.addAll(photos);
                    mPhotosAdapter.notifyDataSetChanged();

                    refreshEmptyView();
                    break;
                case LOADER_ALBUMS:
                    mAlbumsAdapter.swapCursor(cursor);
                    mAlbumsAdapter.notifyDataSetChanged();
                    break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public class MultiChoiceModeListener implements GridView.MultiChoiceModeListener {
        private Map<Long, Boolean> mSelected = new HashMap<Long, Boolean>();

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            int selectCount = mGridView.getCheckedItemCount();
            mode.setSubtitle(getActivity().getResources().getQuantityString(R.plurals.numbersOfPhotosSelected, selectCount, selectCount));
            mSelected.put(id, checked);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.setTitle(R.string.select_photos);
            mode.setSubtitle(getResources().getQuantityString(R.plurals.numbersOfPhotosSelected, 1, 1));
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_photos, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.moveActionItem:
                    final Cursor[] selectedAlbum = new Cursor[1];
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.select_album)
                            .setSingleChoiceItems(mAlbumsAdapter, 0, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    selectedAlbum[0] = (Cursor) mAlbumsAdapter.getItem(which);
                                }
                            })
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    long albumId;
                                    if (selectedAlbum[0] == null)
                                        albumId = mAlbumsAdapter.getItemId(0);
                                    else
                                        albumId = selectedAlbum[0].getLong(QueryAlbums._ID);
                                    for (final Map.Entry<Long, Boolean> m : mSelected.entrySet()) {
                                        if (m.getValue())
                                            PersistenceService.movePhotoToAlbum(getActivity(), m.getKey(), albumId);
                                    }
                                    mode.finish();
                                }
                            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // Do nothing.
                        }
                    }).show();


                    return true;
                case R.id.deleteActionItem:
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.delete_photo)
                            .setMessage(getResources().getQuantityString(R.plurals.numbersOfPhotosWantToBeDeleted, mGridView.getCheckedItemCount()))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick (DialogInterface dialog, int whichButton)
                                {
                                    for (final Map.Entry<Long, Boolean> m : mSelected.entrySet())
                                    {
                                        if (m.getValue())
                                            PersistenceService.deletePhoto(getActivity(), m.getKey());
                                    }
                                    mode.finish();
                                }
                            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).show();
                    return true;
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

        }
    }
}
