package com.bidroid.tripmemories.app.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

/**
 * Created by Edy on 11.04.2014.
 */
public class Utils {
    public static final String KEY_LAST_ALBUM_ID = "LAST_ALBUM_ID";
    private static Utils ourInstance = new Utils();

    private Context mContext;

    public static Utils getInstance() {
        return ourInstance;
    }

    private Utils() {
    }

    public Context getApplicationContext() {
        return mContext;
    }

    public void setContext(Context context) {
        this.mContext = context;
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public void saveLastAlbum(long id) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
        editor.putLong(KEY_LAST_ALBUM_ID, id);
        editor.apply();
    }

    public long getLastAlbum() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return  sharedPreferences.getLong(KEY_LAST_ALBUM_ID, 0);
    }
}
