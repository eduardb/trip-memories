package com.bidroid.tripmemories.app.provider;

/**
 * Created by Edy on 12.04.2014.
 */
public interface QueryPhotos {
    public static final String[] PROJECTION = new String[] {
            PhotosContract._ID,
            PhotosContract.ALBUM_ID,
            PhotosContract.PHOTO_PATH,
            PhotosContract.DATE_CREATED
    };

    int _ID = 0;
    int ALBUM_ID = _ID + 1;
    int PHOTO_PATH = ALBUM_ID + 1;
    int DATE_CREATED = PHOTO_PATH + 1;
}
