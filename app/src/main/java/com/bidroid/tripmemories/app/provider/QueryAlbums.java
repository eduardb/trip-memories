package com.bidroid.tripmemories.app.provider;

/**
 * Created by Edy on 11.04.2014.
 */
public interface QueryAlbums {
    public static final String[] PROJECTION = new String[] {
            AlbumsContract._ID,
            AlbumsContract.ALBUM_NAME,
            AlbumsContract.ALBUM_COVER,
            AlbumsContract.DATE_CREATED
    };

    int _ID = 0;
    int ALBUM_NAME = _ID + 1;
    int ALBUM_COVER = ALBUM_NAME + 1;
    int DATE_CREATED = ALBUM_COVER + 1;
}
