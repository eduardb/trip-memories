package com.bidroid.tripmemories.app.ui.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bidroid.tripmemories.app.R;
import com.squareup.picasso.Picasso;

import java.io.File;

public class ImageFragment extends Fragment {

	public static final String PHOTO_PATH = "photo_path";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.image_fragment, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		String photoPath = null;
		if (getArguments() != null) {
			photoPath = getArguments().getString(PHOTO_PATH);
		}

        Picasso.with(getActivity())
                .load(new File(photoPath))
                .centerInside()
                .fit()
                .into((ImageView) view.findViewById(R.id.image));
	}
}
