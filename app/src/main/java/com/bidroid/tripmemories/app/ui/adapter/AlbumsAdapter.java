package com.bidroid.tripmemories.app.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bidroid.tripmemories.app.R;
import com.bidroid.tripmemories.app.domain.Album;
import com.bidroid.tripmemories.app.ui.custom.CheckableLayout;
import com.bidroid.tripmemories.app.ui.custom.SquaredImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/**
 * Created by Edy on 11.04.2014.
 */
public class AlbumsAdapter extends ArrayAdapter<Album> {

    private final LayoutInflater mInflater;

    public AlbumsAdapter(Context context, List<Album> albums) {
        super(context, 0, albums);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            view = mInflater.inflate(R.layout.album_grid_item, parent, false);
            viewHolder.imageView = (SquaredImageView) view.findViewById(R.id.imageView);
            viewHolder.textView = (TextView) view.findViewById(R.id.textView);
            convertView = new CheckableLayout(getContext());
            convertView.setLayoutParams(new GridView.LayoutParams(GridView.LayoutParams.MATCH_PARENT,
                    GridView.LayoutParams.MATCH_PARENT));
            ((CheckableLayout) convertView).addView(view);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Album album = getItem(position);
        if (album.getAlbumCover() != null)
            Picasso.with(getContext())
                    .load(new File(album.getAlbumCover()))
                    .centerCrop()
                    .fit()
                    .into(viewHolder.imageView);
        else
            viewHolder.imageView.setImageResource(R.drawable.no_picture);
        viewHolder.textView.setText(album.getName());
        return convertView;
    }

    static class ViewHolder {
        SquaredImageView imageView;
        TextView textView;
    }
}
