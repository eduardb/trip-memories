package com.bidroid.tripmemories.app.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Window;

import com.bidroid.tripmemories.app.R;
import com.bidroid.tripmemories.app.ui.fragments.ImageFragment;

import java.util.List;

/**
 * Created by andreicatinean on 11/5/13.
 */
public class PhotosViewerActivity extends FragmentActivity {

    private ViewPager mPager;
    private PagerAdapter mAdapter;
    public static final String PHOTOS_LIST = "id";
    public static final String PHOTOS_LIST_POSITION = "pos";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_photos_viewer);
        List<String> photos = getIntent().getStringArrayListExtra(PHOTOS_LIST);
        int pos = getIntent().getIntExtra(PHOTOS_LIST_POSITION, 0);

        if (photos == null) {
            throw new IllegalArgumentException("photos not supplied");
        }

        mPager = (ViewPager) findViewById(R.id.pager);

        mAdapter = new PhotosPagerAdapter(getSupportFragmentManager(), photos);
        mPager.setAdapter(mAdapter);
        mPager.setCurrentItem(pos - photos.size());
    }

    public static class PhotosPagerAdapter extends FragmentPagerAdapter {

        private List<String> mPhotos;
        public PhotosPagerAdapter(FragmentManager fm){
            super(fm);
        }

        public PhotosPagerAdapter(FragmentManager fm, List<String> photosList){
            this(fm);

            mPhotos = photosList;
        }

        @Override
        public Fragment getItem(int pos) {

            ImageFragment frg = new ImageFragment();
            Bundle args = new Bundle();
            args.putString(ImageFragment.PHOTO_PATH, mPhotos.get(pos));
            frg.setArguments(args);
            return frg;
        }

        @Override
        public int getCount() {
            return mPhotos.size();
        }
    }
}
