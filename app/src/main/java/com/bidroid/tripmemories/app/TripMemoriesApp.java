package com.bidroid.tripmemories.app;

import android.app.Application;

import com.bidroid.tripmemories.app.util.Utils;

/**
 * Created by Edy on 12.04.2014.
 */
public class TripMemoriesApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Utils.getInstance().setContext(getApplicationContext());
    }
}
