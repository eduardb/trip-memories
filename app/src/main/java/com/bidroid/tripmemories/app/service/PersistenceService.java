package com.bidroid.tripmemories.app.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.bidroid.tripmemories.app.provider.AlbumsContract;
import com.bidroid.tripmemories.app.provider.PhotosContract;
import com.bidroid.tripmemories.app.provider.QueryAlbums;
import com.bidroid.tripmemories.app.provider.QueryPhotos;
import com.bidroid.tripmemories.app.util.Utils;

import java.io.File;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 */
public class PersistenceService extends IntentService {
    private static final String ACTION_CREATE_ALBUM = "com.bidroid.tripmemories.app.service.action.create_album";
    public static final  String ACTION_RENAME_ALBUM =  "com.bidroid.tripmemories.app.service.action.rename_album";
    public static final  String ACTION_DELETE_ALBUM =  "com.bidroid.tripmemories.app.service.action.delete_album";

    public static final String ACTION_DELETE_PHOTO = "com.bidroid.tripmemories.app.service.action.delete_photo";
    public static final String ACTION_ADD_PHOTO =  "com.bidroid.tripmemories.app.service.action.add_photo";
    public static final String ACTION_MOVE_PHOTO_TO_ALBUM = "com.bidroid.tripmemories.app.service.action.move_photo_to_album";

    private static final String EXTRA_PARAM_ALBUM_NAME = "com.bidroid.tripmemories.app.service.extra.album_name";
    private static final String EXTRA_PARAM_ALBUM_ID = "com.bidroid.tripmemories.app.service.extra.album_id";
    private static final String EXTRA_PARAM_ALBUM_NEW_NAME = "com.bidroid.tripmemories.app.service.extra.album_new_name";

    private static final String EXTRA_PARAM_PHOTO_ID  = "com.bidroid.tripmemories.app.service.extra.photo_id";
    private static final String EXTRA_PARAM_PHOTO_PATH = "com.bidroid.tripmemories.app.service.extra.photo_path";
    private static final String ORDER_DESC = " DESC";

    /**
     * Starts this service to perform action "create album" with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void createAlbum(Context context, String albumName) {
        Intent intent = new Intent(context, PersistenceService.class);
        intent.setAction(ACTION_CREATE_ALBUM);
        intent.putExtra(EXTRA_PARAM_ALBUM_NAME, albumName);
        context.startService(intent);
    }

    public static void renameAlbum(Context context, long albumId, String newName)
    {
        Intent intent = new Intent(context, PersistenceService.class);
        intent.setAction(ACTION_RENAME_ALBUM);
        intent.putExtra(EXTRA_PARAM_ALBUM_ID, albumId);
        intent.putExtra(EXTRA_PARAM_ALBUM_NEW_NAME, newName);
        context.startService(intent);
    }

    public static void deleteAlbum(Context context, long albumId)
    {
        Intent intent = new Intent(context, PersistenceService.class);
        intent.setAction(ACTION_DELETE_ALBUM);
        intent.putExtra(EXTRA_PARAM_ALBUM_ID, albumId);
        context.startService(intent);
    }

    public static void deletePhoto(Context context, long photoId)
    {
        Intent intent = new Intent(context, PersistenceService.class);
        intent.setAction(ACTION_DELETE_PHOTO);
        intent.putExtra(EXTRA_PARAM_PHOTO_ID, photoId);
        context.startService(intent);
    }

    public static void addPhotoToAlbum(Context context, String photoPath, long albumId) {
        Intent intent = new Intent(context, PersistenceService.class);
        intent.setAction(ACTION_ADD_PHOTO);
        intent.putExtra(EXTRA_PARAM_PHOTO_PATH, photoPath);
        intent.putExtra(EXTRA_PARAM_ALBUM_ID, albumId);
        context.startService(intent);
    }

    public static void movePhotoToAlbum (Context context, long photoId, long albumId){
        Intent intent = new Intent(context, PersistenceService.class);
        intent.setAction(ACTION_MOVE_PHOTO_TO_ALBUM);
        intent.putExtra(EXTRA_PARAM_PHOTO_ID, photoId);
        intent.putExtra(EXTRA_PARAM_ALBUM_ID, albumId);
        context.startService(intent);

    }

    public PersistenceService() {
        super("PersistenceService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_CREATE_ALBUM.equals(action)) {
                final String album_name = intent.getStringExtra(EXTRA_PARAM_ALBUM_NAME);
                handleActionCreateAlbumAction(album_name);
            }
            else if(ACTION_RENAME_ALBUM.equals(action))
            {
                final long albumId = intent.getLongExtra(EXTRA_PARAM_ALBUM_ID, -1);
                final String newName = intent.getStringExtra(EXTRA_PARAM_ALBUM_NEW_NAME);
                handleActionRenameAlbum(albumId, newName);
            }
            else if(ACTION_DELETE_ALBUM.equals(action))
            {
                final long albumId = intent.getLongExtra(EXTRA_PARAM_ALBUM_ID, -1);
                handleActionDeleteAlbum(albumId);
            }
            else if(ACTION_DELETE_PHOTO.equals(action))
            {
                final long photoId = intent.getLongExtra(EXTRA_PARAM_PHOTO_ID, -1);
                handleActionDeletePhoto(photoId);
            }
            else if(ACTION_ADD_PHOTO.equals(action))
            {
                final  String photoPath = intent.getStringExtra(EXTRA_PARAM_PHOTO_PATH);
                final  long albumId = intent.getLongExtra(EXTRA_PARAM_ALBUM_ID, -1);
                handleActionAddPhotoToAlbum(photoPath, albumId);
            } else if (ACTION_MOVE_PHOTO_TO_ALBUM.equals(action)) {
                final long photoId = intent.getLongExtra(EXTRA_PARAM_PHOTO_ID, -1);
                final long albumId = intent.getLongExtra(EXTRA_PARAM_ALBUM_ID, -1);
                handleActionMovePhotoToAlbum(photoId, albumId);
            }
        }
    }

    private void handleActionRenameAlbum(long albumId, String newName) {
        ContentValues cv = new ContentValues();
        cv.put(AlbumsContract.ALBUM_NAME, newName);

        getContentResolver().update(AlbumsContract.CONTENT_URI, cv,
                AlbumsContract._ID + "=?", new String[]{""+albumId});
    }

    private void handleActionDeleteAlbum(long albumId) {
        getContentResolver().delete(AlbumsContract.CONTENT_URI,
                AlbumsContract._ID + "=?", new String[]{"" + albumId});

        Cursor cursor = getContentResolver().query(PhotosContract.CONTENT_URI, QueryPhotos.PROJECTION,
                PhotosContract.ALBUM_ID + "=?",new String[] {albumId +""}, null);

        while (cursor != null && cursor.moveToNext())
        {
            File file = new File(cursor.getString(QueryPhotos.PHOTO_PATH));
            if(file.exists())
                file.delete();
        }
        if (cursor != null)
            cursor.close();

        getContentResolver().delete(PhotosContract.CONTENT_URI,
                PhotosContract.ALBUM_ID + "=?", new String[]{"" + albumId});

        if (albumId == Utils.getInstance().getLastAlbum()) {
            // seteaza cel mai recent album dupa ce s-a sters cel dat
            Cursor c = getContentResolver().query(AlbumsContract.CONTENT_URI, QueryAlbums.PROJECTION,
                    null, null, AlbumsContract.DATE_CREATED + ORDER_DESC);
            if (c != null && c.moveToNext()) {
                Utils.getInstance().saveLastAlbum(c.getLong(QueryAlbums._ID));
            }
            if (c != null)
                cursor.close();
        }
    }

    /**
     * Handle action "create album" in the provided background thread with the provided
     * parameters.
     */
    private void handleActionCreateAlbumAction(String albumName) {
        ContentValues cv = new ContentValues();
        cv.put(AlbumsContract.ALBUM_NAME, albumName);
        cv.put(AlbumsContract.DATE_CREATED, System.currentTimeMillis());
        cv.put(AlbumsContract.ALBUM_COVER, (String)null);

        Uri uri = getContentResolver().insert(AlbumsContract.CONTENT_URI, cv);

        Utils.getInstance().saveLastAlbum(Long.parseLong(uri.getLastPathSegment()));
    }

    private void handleActionDeletePhoto(long photoId) {

        Cursor cursor = getContentResolver().query(PhotosContract.CONTENT_URI, QueryPhotos.PROJECTION,
                PhotosContract._ID + "=?",new String[] {photoId +""}, null);

        long albumId = -1;

        if (cursor != null && cursor.moveToNext())
        {
            File file = new File(cursor.getString(QueryPhotos.PHOTO_PATH));
            if(file.exists())
                file.delete();

            albumId = cursor.getLong(QueryPhotos.ALBUM_ID);
            cursor.close();
        }

        getContentResolver().delete(PhotosContract.CONTENT_URI,
                PhotosContract._ID + "=?", new String[]{"" + photoId});

        updateAlbumCover(albumId);
    }

    private void handleActionAddPhotoToAlbum(String photoPath, long albumId) {
        ContentValues cv = new ContentValues();
        cv.put(PhotosContract.PHOTO_PATH, photoPath);
        cv.put(PhotosContract.ALBUM_ID, albumId);
        cv.put(PhotosContract.DATE_CREATED, System.currentTimeMillis());

        getContentResolver().insert(PhotosContract.CONTENT_URI, cv);

        cv.clear();
        cv.put(AlbumsContract.ALBUM_COVER, photoPath);
        getContentResolver().update(AlbumsContract.CONTENT_URI, cv,
                AlbumsContract._ID + "=?", new String[]{"" + albumId});
    }

    private void handleActionMovePhotoToAlbum(long photoId, long newAlbumId) {
        long oldAlbumId = -1;

        Cursor c = getContentResolver().query(PhotosContract.CONTENT_URI, QueryPhotos.PROJECTION,
                PhotosContract._ID + "=?", new String[] {photoId + ""}, null);

        if (c.moveToNext()) {
            oldAlbumId = c.getLong(QueryPhotos.ALBUM_ID);
        }
        c.close();

        ContentValues cv = new ContentValues();
        cv.put(PhotosContract.ALBUM_ID, newAlbumId);

        getContentResolver().update(PhotosContract.CONTENT_URI, cv,
                PhotosContract._ID + "=?", new String[]{""+photoId});


        updateAlbumCover(oldAlbumId);
        updateAlbumCover(newAlbumId);

    }

    private void updateAlbumCover(long albumId) {
        ContentValues cv =new ContentValues() ;
        Cursor cursor = getContentResolver().query(PhotosContract.CONTENT_URI, QueryPhotos.PROJECTION,
                PhotosContract.ALBUM_ID + "=?", new String[] {albumId + ""}, PhotosContract.DATE_CREATED + ORDER_DESC);

        cv.clear();

        if (cursor != null && cursor.moveToNext()) {
            cv.put(AlbumsContract.ALBUM_COVER, cursor.getString(QueryPhotos.PHOTO_PATH));
            getContentResolver().update(AlbumsContract.CONTENT_URI, cv,
                    AlbumsContract._ID + "=?", new String[]{"" + albumId});

            cursor.close();
        } else {
            cv.put(AlbumsContract.ALBUM_COVER, (String)null);
            getContentResolver().update(AlbumsContract.CONTENT_URI, cv,
                    AlbumsContract._ID + "=?", new String[]{albumId+""});
        }
    }

}
