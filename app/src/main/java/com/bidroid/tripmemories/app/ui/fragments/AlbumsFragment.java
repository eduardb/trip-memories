package com.bidroid.tripmemories.app.ui.fragments;

/**
 * Created by Edy on 11.04.2014.
 */

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import com.bidroid.tripmemories.app.R;
import com.bidroid.tripmemories.app.domain.Album;
import com.bidroid.tripmemories.app.provider.AlbumsContract;
import com.bidroid.tripmemories.app.provider.QueryAlbums;
import com.bidroid.tripmemories.app.service.PersistenceService;
import com.bidroid.tripmemories.app.ui.MainActivity;
import com.bidroid.tripmemories.app.ui.adapter.AlbumsAdapter;
import com.bidroid.tripmemories.app.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A placeholder fragment containing a simple view.
 */
public class AlbumsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    public static final String ORDER_DESC = " DESC";
    private GridView mGridView;
    private AlbumsAdapter mAlbumsAdapter;
    private View mEmptyView;

    public AlbumsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(R.string.albums);
        getActivity().getActionBar().setHomeButtonEnabled(false);
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);
        getActivity().getActionBar().setHomeButtonEnabled(false);

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        return rootView;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mGridView = (GridView) view.findViewById(R.id.gridview);

        List<Album> albums = new ArrayList<Album>();

        mAlbumsAdapter = new AlbumsAdapter(getActivity(), albums);
        mGridView.setAdapter(mAlbumsAdapter);

        mGridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
        mGridView.setMultiChoiceModeListener(new MultiChoiceModeListener());

        mEmptyView = view.findViewById(R.id.emptyView);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Utils.getInstance().saveLastAlbum(id);
                FragmentTransaction tr = getActivity().getFragmentManager().beginTransaction().addToBackStack(null);
                tr.replace(R.id.container, PhotosFragment.newInstance(id+""));
                tr.commit();
            }
        });

        restartLoader();
    }


    public void refreshEmptyView()
    {
        if(mAlbumsAdapter.getCount() == 0)
        {
            mEmptyView.setVisibility(View.VISIBLE);
        }
        else
            mEmptyView.setVisibility(View.GONE);
    }

    private void restartLoader() {
        getLoaderManager().restartLoader(1, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (getActivity() == null) {
            return null;
        }

        Loader<Cursor> loader = null;
        String selection = null;
        String[] selectionArgs = null;
        switch (id) {
            case 1:
                loader = new CursorLoader(getActivity(),
                        AlbumsContract.CONTENT_URI,
                        QueryAlbums.PROJECTION, selection, selectionArgs,
                        AlbumsContract.DATE_CREATED + ORDER_DESC);
                break;
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (getActivity() == null) {
            return;
        }

        if (cursor != null && !cursor.isClosed()) {
            switch (cursorLoader.getId()) {
                case 1:
                    List<Album> albums = new ArrayList<Album>();
                    while (cursor.moveToNext()) {
                        Album album = new Album();
                        album.setName(cursor.getString(QueryAlbums.ALBUM_NAME));
                        album.setAlbumCover(cursor.getString(QueryAlbums.ALBUM_COVER));
                        album.setId(cursor.getLong(QueryAlbums._ID));
                        album.setDateCreated(cursor.getLong(QueryAlbums.DATE_CREATED));
                        albums.add(album);
                    }
                    mAlbumsAdapter.clear();
                    mAlbumsAdapter.addAll(albums);
                    mAlbumsAdapter.notifyDataSetChanged();
                    break;
            }
        }
        refreshEmptyView();

       ((MainActivity) getActivity()).mHasAlbum = mAlbumsAdapter.getCount() > 0;
        getActivity().invalidateOptionsMenu();

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void renameItem(final long albumId, final int position)
    {
        final EditText input = new EditText(getActivity());
        input.setSingleLine();
        input.setText(mAlbumsAdapter.getItem(position).getName());
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.album_name)
                .setMessage(R.string.new_name)
                .setView(input)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Editable value = input.getText();
                                if (value.toString().equals("")) {
                                    Toast.makeText(getActivity(), R.string.album_name_empty, Toast.LENGTH_SHORT).show();
                                    renameItem(albumId, position);
                                } else {
                                    PersistenceService.renameAlbum(getActivity(), albumId, value.toString());
                                }
                            }
                        }
                ).

                    setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    // Do nothing.
                                }
                            }
                    ).

                    show();
                }


        public class MultiChoiceModeListener implements GridView.MultiChoiceModeListener {
        private Map<Long, Boolean> mSelected = new HashMap<Long, Boolean>();
        private int mSelectedPosition = -1;

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            int selectCount = mGridView.getCheckedItemCount();
            mode.setSubtitle(getActivity().getResources().getQuantityString(R.plurals.numberOfAlbumsSelected, selectCount, selectCount));

            if (mGridView.getCheckedItemCount() == 1 && checked)
                mSelectedPosition = position;

            mSelected.put(id, checked);
            mode.invalidate();
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.setTitle(R.string.select_albums);
            mode.setSubtitle(getResources().getQuantityString(R.plurals.numberOfAlbumsSelected, 1, 1));
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_albums, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            menu.findItem(R.id.renameActionItem).setVisible(mGridView.getCheckedItemCount() == 1);
            return true;
        }

        @Override
        public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.renameActionItem:
                    for (final Map.Entry<Long, Boolean> m : mSelected.entrySet())
                    {
                        if (m.getValue()) {
                            renameItem(m.getKey(), mSelectedPosition);
                            mode.finish();
                            break;
                        }
                    }
                    return true;
                case R.id.deleteActionItem:
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.delete_album)
                            .setMessage(getResources().getQuantityString(R.plurals.numberOfAlbumsWantToBeDeleted, mGridView.getCheckedItemCount()))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick (DialogInterface dialog, int whichButton)
                                {
                                    for (final Map.Entry<Long, Boolean> m : mSelected.entrySet())
                                    {
                                        if (m.getValue())
                                            PersistenceService.deleteAlbum(getActivity(), m.getKey());
                                    }
                                    mode.finish();
                                }
                            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).show();
                    return true;
            }

            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

        }
    }
}