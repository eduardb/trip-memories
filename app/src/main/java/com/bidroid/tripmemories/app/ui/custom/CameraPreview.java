package com.bidroid.tripmemories.app.ui.custom;

/**
 * Created by Edy on 11.04.2014.
 */

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

/** A basic Camera preview class */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private static final String TAG = "CameraPreview";
    private SurfaceHolder mHolder;
    private Camera mCamera;

    public CameraPreview(Context context, Camera camera) {
        super(context);
        mCamera = camera;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // empty. Take care of releasing the Camera preview in your activity.
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        Camera.Parameters parameters = mCamera.getParameters();

        Camera.Size previewSize = getOptimalPreviewSize(parameters.getSupportedPreviewSizes(), w, h);

        parameters.setPreviewSize(previewSize.width, previewSize.height);
        if (parameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        else if (parameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO))
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        else if (parameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_INFINITY))
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);

        Camera.Size pictureSize = getHighestPictureSize(parameters.getSupportedPictureSizes());
        parameters.setPictureSize(pictureSize.width, pictureSize.height);

        mCamera.setParameters(parameters);

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e) {
            Log.d(TAG, String.format("Error starting camera preview: %s", e.getMessage()));
        }
    }

    public static Camera.Size getHighestPictureSize(List<Camera.Size> sizes) {
        long highestSizeValue = 0;
        Camera.Size highestSize = sizes.get(0);
        for (Camera.Size size : sizes) {
            if (size.height * size.width > highestSizeValue) {
                highestSizeValue = size.height * size.width;
                highestSize = size;
            }
        }
        return highestSize;
    }

    public static Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) w / h;
        // next line added because of some confusion between landscape and portrait - works for testing, at least!
        if (targetRatio < 1.0)
            targetRatio = 1.0/targetRatio;
        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;
//   Log.d(TAG, String.format("targetRatio=%f", targetRatio));

        // Try to find an size match aspect ratio and size
        for (Camera.Size size : sizes) {
//           Log.d(TAG, String.format("Trying: w=%d h=%d", size.width, size.height));
            double ratio = (double) size.width / size.height;
//       Log.d(TAG, String.format("ratio=%f", ratio));
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio, ignore the requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

}