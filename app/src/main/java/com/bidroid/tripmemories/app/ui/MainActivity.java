package com.bidroid.tripmemories.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.bidroid.tripmemories.app.R;
import com.bidroid.tripmemories.app.service.PersistenceService;
import com.bidroid.tripmemories.app.ui.fragments.AlbumsFragment;
import com.bidroid.tripmemories.app.util.Utils;


public class MainActivity extends Activity {

    public boolean mHasAlbum;
    public boolean mInAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new AlbumsFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_create_album) {
            createAlbumDialog();
        } else if (id == R.id.action_new_photo) {

            if (!Utils.getInstance().isExternalStorageWritable())
                Toast.makeText(this, R.string.external_storage_warning, Toast.LENGTH_LONG).show();
            else {
                Intent intentToCamera = new Intent(this, CameraActivity.class);
                startActivity(intentToCamera);
            }
        } else if (id == android.R.id.home) {
            getFragmentManager().popBackStack();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createAlbumDialog() {
        final EditText input = new EditText(this);
        input.setSingleLine();
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(R.string.album_name)
                .setMessage(R.string.new_name)
                .setView(input)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Editable value = input.getText();
                        if (value.toString().equals("")) {
                            Toast.makeText(MainActivity.this, R.string.album_name_empty, Toast.LENGTH_SHORT).show();
                            createAlbumDialog();
                        } else {
                            PersistenceService.createAlbum(MainActivity.this, value.toString());
                        }
                    }
                }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Do nothing.
            }
        }).show();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(!mHasAlbum)
            menu.removeItem(R.id.action_new_photo);
        if(mInAlbum)
             menu.removeItem(R.id.action_create_album);
        return super.onPrepareOptionsMenu(menu);
    }
}
