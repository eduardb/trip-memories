package com.bidroid.tripmemories.app.provider;

import android.net.Uri;

import com.tjeannin.provigen.Constraint;
import com.tjeannin.provigen.ProviGenBaseContract;
import com.tjeannin.provigen.annotation.Column;
import com.tjeannin.provigen.annotation.ContentUri;
import com.tjeannin.provigen.annotation.Contract;
import com.tjeannin.provigen.annotation.NotNull;

/**
 * Created by Edy on 11.04.2014.
 */
@Contract(version = 1)
public interface PhotosContract extends ProviGenBaseContract {

    @Column(Column.Type.TEXT)
    @NotNull(Constraint.OnConflict.ABORT)
    public static final String PHOTO_PATH = "photo_path";

    @Column(Column.Type.INTEGER)
    @NotNull(Constraint.OnConflict.ABORT)
    public static final String DATE_CREATED = "date_created";

    @Column(Column.Type.INTEGER)
    @NotNull(Constraint.OnConflict.ABORT)
    public static final String ALBUM_ID = "album_id";

    @ContentUri
    public static final Uri CONTENT_URI = Uri.parse("content://com.bidroid.tripmemories.app/photos");
}