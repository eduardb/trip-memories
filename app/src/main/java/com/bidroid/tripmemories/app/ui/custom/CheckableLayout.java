package com.bidroid.tripmemories.app.ui.custom;

import android.content.Context;
import android.os.Build;
import android.widget.Checkable;
import android.widget.FrameLayout;

import com.bidroid.tripmemories.app.R;

/**
 * Created by Edy on 12.04.2014.
 */
public class CheckableLayout extends FrameLayout implements Checkable {
    private boolean mChecked;

    public CheckableLayout(Context context) {
        super(context);
    }

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
        if (Build.VERSION.SDK_INT >= 16)
            setBackground(checked ?
                    getResources().getDrawable(R.drawable.blue_box)
                    : null);
        else
            setBackgroundDrawable(checked ?
                    getResources().getDrawable(R.drawable.blue_box)
                    : null);
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

}