package com.bidroid.tripmemories.app.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bidroid.tripmemories.app.R;
import com.bidroid.tripmemories.app.domain.Photo;
import com.bidroid.tripmemories.app.ui.custom.CheckableLayout;
import com.bidroid.tripmemories.app.ui.custom.SquaredImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Edy on 12.04.2014.
 */
public class PhotosAdapter extends ArrayAdapter<Photo> {
    public static final String DATE_FORMATER = "yyyy-MM-dd HH:mm";
    private final LayoutInflater mInflater;
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMATER);

    public PhotosAdapter(Context context, List<Photo> photos) {
        super(context, 0, photos);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            view = mInflater.inflate(R.layout.album_grid_item, parent, false);
            viewHolder.imageView = (SquaredImageView) view.findViewById(R.id.imageView);
            viewHolder.textView = (TextView) view.findViewById(R.id.textView);
            convertView = new CheckableLayout(getContext());
            convertView.setLayoutParams(new GridView.LayoutParams(GridView.LayoutParams.MATCH_PARENT,
                    GridView.LayoutParams.MATCH_PARENT));
            ((CheckableLayout) convertView).addView(view);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Photo photo = getItem(position);

        Picasso.with(getContext())
                .load(new File(photo.getPath()))
                .centerCrop()
                .fit()
                .into(viewHolder.imageView);

        viewHolder.textView.setText(simpleDateFormat.format(new Date(photo.getDateCreated())));
        return convertView;
    }

    static class ViewHolder {
        SquaredImageView imageView;
        TextView textView;
    }
}
