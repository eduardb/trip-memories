package com.bidroid.tripmemories.app.ui;

import android.app.Activity;

import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.bidroid.tripmemories.app.R;
import com.bidroid.tripmemories.app.service.PersistenceService;
import com.bidroid.tripmemories.app.ui.custom.CameraPreview;
import com.bidroid.tripmemories.app.util.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Roxana on 11.04.2014.
 */
public class CameraActivity extends Activity implements Camera.PictureCallback {
    final static String DEBUG_TAG = "CameraActivity";
    public static final String DATE_FORMATER = "yyyyMMddhhmmss";
    private Camera mCamera;
    private CameraPreview mPreview;
    private  boolean mCameraStarted;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_camera);

        // Create an instance of Camera
        mCamera = getCameraInstance();


        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
    }

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    public void onClick(View view) {
        if(mCameraStarted)
            return;
        mCameraStarted = true;
        mCamera.takePicture(null, null, this);
        }

    @Override
    protected void onPause() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
        super.onPause();
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        File pictureFileDir = getDir();

        if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {

            Log.d(CameraActivity.DEBUG_TAG, "Can't create directory to save image.");
            Toast.makeText(this, R.string.cant_create_directory_images,
                    Toast.LENGTH_LONG).show();
            return;

        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMATER);
        String date = dateFormat.format(new Date());
        String photoFile = "Picture_" + date + ".jpg";

        String filename = pictureFileDir.getPath() + File.separator + photoFile;

        File pictureFile = new File(filename);

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();

            Toast.makeText(this, R.string.new_photo_saved, Toast.LENGTH_LONG).show();

            PersistenceService.addPhotoToAlbum(this, filename, Utils.getInstance().getLastAlbum());
            CameraActivity.this.finish();
        } catch (Exception error) {
            Log.d(CameraActivity.DEBUG_TAG, "File" + filename + "not saved: "
                    + error.getMessage());
            Toast.makeText(this, R.string.image_could_not_be_saved,
                    Toast.LENGTH_LONG).show();
        }
    }

    private File getDir() {
        File sdDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return sdDir;
    }
}
